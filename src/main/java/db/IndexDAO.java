package db;

import java.sql.*;
import java.util.Map;
import java.util.Set;

public class IndexDAO {

    public static void persistIndex(Map<String, Set<String>> index) {

        Connection conn = null;
        boolean wasError = false;


        try {

            conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/URL","sa","");
            conn.setAutoCommit(false);

             Statement stat = conn.createStatement();
             stat.executeQuery("SELECT*FROM INDEX");
             ResultSet rs = stat.getResultSet();

             if(!rs.next() ) {
                 String sql = "INSERT INTO index(WORD,LINK) VALUES (?, ?)";

                 PreparedStatement st = conn.prepareStatement(sql);

                 for (String word : index.keySet()) {
                     for (String link : index.get(word)) {
                         st.setString(1, word);
                         st.setString(2, link);
                         st.executeUpdate();

                     }
                 }
             }
        }catch (SQLException ex){

            wasError = true;
            ex.printStackTrace();
            try {
                conn.rollback();

            }catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {

            if(conn != null) {

                try{
                    if (wasError == false ) {
                        conn.commit();
                        conn.close();
                    }
                }catch(SQLException ignore) {

                }

            }
     }

 }

   public static void readingIndex() {

       Connection conn = null;
       boolean wasError = false;

       try {

           conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/URL", "sa", "");
           conn.setAutoCommit(false);

           Statement st = conn.createStatement();
           ResultSet rs = st.executeQuery("SELECT*FROM index");
           ResultSetMetaData md = rs.getMetaData();

           for (int i = 1; i <= md.getColumnCount(); i++) {
               System.out.print(md.getColumnName(i) + "          " +  "\t\t");
           }
           System.out.println();

           while (rs.next()) {
               for (int i = 1; i <= md.getColumnCount(); i++) {
                   System.out.print(rs.getString(i) + "          " + "\t\t");
               }
               System.out.println();
           }


       } catch (SQLException e) {

           wasError = true;
           e.printStackTrace();

           try {
               conn.rollback();


           } catch (SQLException ex) {

               ex.printStackTrace();
           }


       } finally {
           if (conn != null ) {
               try{
                   if(wasError == false){
                       conn.commit();
                       conn.close();
                   }
               }catch(SQLException ignore){}
           }
       }

   }
}
