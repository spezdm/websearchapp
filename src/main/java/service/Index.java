package service;

import java.util.*;

public class Index {

    private Map<String, Set<String>> index = new HashMap<>();




    public void addToIndex(String keyword, String url) {

        Set<String> links = index.get(keyword);
        if (links == null) {
            links = new HashSet<>();
        }
        links.add(url);

        index.put(keyword, links);
    }


    public void addToIndex(List<String> keywords, String url) {

        for (String key : keywords) {
            addToIndex(key, url);

        }
    }

    public  Map<String, Set<String>> getIndex() {
        return index;
    }


    public  Set<String> getUrlsOnWord(String word) {
      Set<String> s;
      for(String k : index.keySet()) {

          if(k.equals(word)) {

                s = index.get(k);
                return s;
          }
        }

        return null;
    }
}
