package service;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WebCrawler {

    public static String internalUrl;
    public static String startPage ;
    Index idx;

    public WebCrawler(String url) {
        Pattern pattern = Pattern.compile("(https?://)([^:^/]*)(:\\d*)?(.*)?");
        Matcher matcher = pattern.matcher(url);
        matcher.find();

        String protocol = matcher.group(1);
        String domain = matcher.group(2);
        String port = matcher.group(3);
        String uri = matcher.group(4);

        internalUrl = protocol+domain;
        startPage = uri;

        idx = new Index();

    }

    public  void crawl() {

        String startUrl = internalUrl + startPage;
        idx = new Index();

        LinkedList<String> pagesToVisit = new LinkedList<>();
        LinkedList<String> visitedLinked = new LinkedList<>();
        pagesToVisit.add(startUrl);

        while (pagesToVisit.size() > 0) {

            String nextUrl = pagesToVisit.removeFirst();
            visitedLinked.add(nextUrl);

            StringBuilder pageContent = Parser.getPageContent(nextUrl);
            List<String> words = Parser.getPageToken(pageContent);
            idx.addToIndex(words, nextUrl);


            List<String> newLinks = Parser.getAllLinkOnPage(pageContent);

            for (String l : newLinks) {

                if (!visitedLinked.contains(l)) {
                    pagesToVisit.addLast(l);
                }

            }


        }

        //idx.getIndex();
        //idx.getUrlsOnWord("Kick");
      //  IndexDAO.persistIndex(idx.getIndex());
      //  IndexDAO.readingIndex();


    }
    public Set<String> qurey(String word) {
        return  idx.getUrlsOnWord(word);
    }
}