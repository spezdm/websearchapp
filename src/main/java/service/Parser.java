package service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Parser {


    public static void printLinks(List<String> stringList) {
        int count = 0;
        Iterator<String> stringItr = stringList.iterator();
        while (stringItr.hasNext()) {
            System.out.println(stringItr.next());
            count++;
        }

        System.out.println("---------------------------------");
        System.out.println("Количество ссылок: " + count);
        System.out.println();
    }

    public static List<String> getLinkOnPage(StringBuilder pageContent) {


        int startPosArray = pageContent.indexOf("href=\"https:") + 6;
        int endPosArray = pageContent.indexOf("\"", startPosArray);
        List<String> lh = new ArrayList<>();
        lh.add(pageContent.substring(startPosArray, endPosArray));
        return lh;

    }

    public static List<String> getAllLinkOnPage(StringBuilder pageContent) {

        pageContent = new StringBuilder(pageContent.substring(0, pageContent.length()));
        List<String> lh1 = new ArrayList<>();
        for (int i = 0; i < pageContent.length(); i++) {
            if (pageContent.indexOf("href=\"https:") != -1) {
                int startPosArray = pageContent.indexOf("href=\"https:") + 6;
                int endPosArray = pageContent.indexOf("\"", startPosArray);
                lh1.add(pageContent.substring(startPosArray, endPosArray));
                pageContent.delete(0, endPosArray);

            }
        }

        return lh1;
    }

    public static String removeTags(String string) {
        String str;
        Pattern REMOVE_TAGS = Pattern.compile("<.+?>");
        Matcher m = REMOVE_TAGS.matcher(string);
        return m.replaceAll("").trim();

    }

    public static List<String> getPageToken(StringBuilder pageContent) {
        pageContent = new StringBuilder(pageContent.substring(0, pageContent.length()));
        String[] tokens = pageContent.toString().split("\\n");


        List<String> result = new ArrayList<>();

        for (String token : tokens) {

            token = removeTags(token);

            if (!token.isEmpty()) {
                result.add(token);

            }
        }

        result = Arrays.asList(result.toString().replaceAll("\\p{Punct}","").split("\\s"));


        return result;
    }

    public static StringBuilder getPageContent(String url) {

        StringBuilder sb = new StringBuilder();
        URL startPage = null;
        try {
            startPage = new URL(url);

        } catch (MalformedURLException e) {
            return new StringBuilder();
        }

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(startPage.openStream()));

            String s;
            while ((s = br.readLine()) != null) {
                sb.append(s + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb;

    }


}
